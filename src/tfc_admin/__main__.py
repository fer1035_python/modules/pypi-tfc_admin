#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""Make Terraform Cloud API calls for common platform administration tasks."""
import __init__

if __name__ == "__main__":
    __init__.main()
